FROM alpine:3.12.1
# install system packages
RUN apk add --update-cache --no-cache git python3-dev py3-pip openblas-dev lapack-dev libc-dev cmake make zlib-dev wget bash
# install python dependencies
RUN pip3 install tabulate biopython cgecore gitpython python-dateutil
# install resfinder
RUN git clone https://git@bitbucket.org/genomicepidemiology/resfinder.git
# install kma
RUN cd /resfinder/cge && git clone https://bitbucket.org/genomicepidemiology/kma.git && cd kma && make
# install blast
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.32-r0/glibc-2.32-r0.apk && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.32-r0/glibc-bin-2.32-r0.apk && apk add --no-cache glibc-2.32-r0.apk && apk add --no-cache glibc-bin-2.32-r0.apk
RUN wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ncbi-blast-2.11.0+-x64-linux.tar.gz 
RUN tar xfz ncbi-blast-2.11.0+-x64-linux.tar.gz  && rm ncbi-blast-2.11.0+-x64-linux.tar.gz  && mv /ncbi-blast-2.11.0+ /ncbi-blast
# install databases
ENV PATH /resfinder/cge/kma:/ncbi-blast/bin:/$PATH
RUN cd /resfinder && git clone https://git@bitbucket.org/genomicepidemiology/resfinder_db.git db_resfinder && cd db_resfinder && python3 INSTALL.py
RUN cd /resfinder && git clone https://git@bitbucket.org/genomicepidemiology/pointfinder_db.git db_pointfinder && cd db_pointfinder && python3 INSTALL.py
RUN cd /resfinder && git clone https://git@bitbucket.org/genomicepidemiology/disinfinder_db.git db_disinfinder && cd db_disinfinder && python3 INSTALL.py

ENV PATH /resfinder/:$PATH
