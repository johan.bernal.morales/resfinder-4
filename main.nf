nextflow.enable.dsl=2

params.reads = false
params.fastas = false
params.output_dir = false
params.species = ""
params.coverage_threshold = 0.9
params.identity_threshold = 0.9

include { RUN_RESFINDER_FASTQS; RUN_RESFINDER_FASTAS; COMBINE_RESFINDER_RESULTS} from './modules/resfinder.nf'
workflow {
  if (params.reads && params.output_dir ){
    reads = Channel
    .fromFilePairs(params.reads)
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
    
    RUN_RESFINDER_FASTQS(reads, params.species, params.coverage_threshold, params.identity_threshold)
    COMBINE_RESFINDER_RESULTS(RUN_RESFINDER_FASTQS.out.collect())
  } else if (params.fastas && params.output_dir ) {
    fastas = Channel
    .fromPath(params.fastas)
    .map{ file -> tuple (file.baseName.replaceAll(/\..+$/,''), file)}
    .ifEmpty { error "Cannot find any fastas matching: ${params.fastas}" }
    
    RUN_RESFINDER_FASTAS(fastas, params.species, params.coverage_threshold, params.identity_threshold)
    COMBINE_RESFINDER_RESULTS(RUN_RESFINDER_FASTAS.out.tsv_result.collect())

  } else {
      error "Please specify reads or fastas and an output directory with --reads or --fastas and --output_dir"
  }
}